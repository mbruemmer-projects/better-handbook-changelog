#!/usr/bin/env python3

import gitlab
import json
import os
import argparse
import re
import urllib.parse
import time
import markdown
from datetime import datetime, timedelta, timezone
from mako.template import Template

def parse_diff(diff):
    diff_size = diff[2:]
    diff_start = diff_size.find("@@")
    diff_size = diff_size[0:diff_start].strip()
    diff_size = diff_size.split(" ")
    return diff_size

def markdown2html(desc_string):
    #desc_string = re.sub("<img.*>","", desc_string)
    if not desc_string:
        return ""
    clean_desc = ""
    desc_string = desc_string.replace("## Why is this change being made?"," ").strip()
    for line in desc_string.splitlines():
        if "## Author Checklist" in line:
            break
        else:
            clean_desc += line + "\n"
    md = markdown.Markdown()
    clean_desc = md.convert(clean_desc)
    return clean_desc

def split_path(changed_files):
    parts = set()
    for changed_file in changed_files:
        changed_file = changed_file[changed_file.find("source/handbook/")+16:]
        path_parts = changed_file.split("/")
        temp_path = ""
        for part in path_parts:
            if ".html.md" not in part:
                temp_path += "/" + part
                parts.add(temp_path)
    return list(parts)

def clean_paths(paths):
    links = []
    for path in paths:
        path = path[path.find("source/handbook/")+16:]
        if path.endswith("/index.html.md"):
            path = path[0:-14]
        elif path.endswith("/index.html.md.erb"):
            path = path[0:-18]
        links.append(path)
    return links

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('-d','--startdate', help='Start date in yyyy-mm-dd')
args = parser.parse_args()

gl = gitlab.Gitlab("https://gitlab.com/", retry_transient_errors=True)

tz = datetime.now().astimezone().tzinfo

start = args.startdate
if not start:
    start = datetime.now(timezone.utc) - timedelta(days=14)
else:
    start = datetime.strptime(args.startdate, '%Y-%m-%d')
    start = start.replace(tzinfo=timezone.utc)

project = gl.projects.get(7764)
mergerequests = project.mergerequests.list(iterator=True, state='merged', sort="desc", per_page=100)
mrs = []

for mr in mergerequests:
    #this excludes some MRs, like 54443
    if mr.attributes["merged_at"]:
        if datetime.strptime(mr.attributes["merged_at"], '%Y-%m-%dT%H:%M:%S.%f%z') < start:
            break
    try:
        changes = mr.changes()
    except:
        print("Could not get changes for %s" % mr.id)
        continue
    change_file_list = []
    change_sizes = []
    for change in changes["changes"]:
        if ( "handbook" in change["new_path"] and
            "index.html.md" in change["new_path"] and
            "no changelog" not in changes["labels"] and
            "backstage" not in changes["labels"] ):
            change_file_list.append(change["new_path"])
        #else not in handbook

    if change_file_list:
        result_mr = {}
        result_mr["url"] = mr.attributes["web_url"]
        result_mr["title"] = mr.attributes["title"]
        result_mr["merged_at"] = mr.attributes["merged_at"]
        result_mr["author_user"] = mr.attributes["author"]["username"]
        result_mr["author_name"] = mr.attributes["author"]["name"]
        result_mr["description"] = markdown2html(mr.attributes["description"])
        #result_mr["paths"] = split_path(change_file_list)
        result_mr["links"] = clean_paths(change_file_list)
        result_mr["total_changes"] = project.commits.get(mr.attributes["merge_commit_sha"]).attributes["stats"]["total"]
        mrs.append(result_mr)

with open('changes.json', 'w') as outfile:
    json.dump(mrs, outfile, indent=4)

#TODO: add dates
#TODO: persists as json and load from there for history and to restrict number of api calls
mytemplate = Template(filename='template/index.html')

with open("public/index.html","w") as outfile:
    outfile.write(mytemplate.render(merge_requests = mrs))