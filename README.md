# better-handbook-changelog

Provide a better changelog for the GitLab handbook.

Full text searchable on title, description and changed pages, fully linked and displaying the changeset size.

## Usage

`python3 generate_changelog.py`

### Parameters

* `-d`, `--startdate`: start date from which onwards changes should be crawled in yyyy-mm-dd. Defaults to today - 7 days
